from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

from enum import Enum, auto

class JC(Enum):
    PFlow = auto()
    LargeR = auto()

VARIATIONS={"JET_GroupedNP_1_UP"}

###############################################################
# Function to apply jet systematics
###############################################################

def applyJetSys(
        sys_list,
        jet_collection,
        jet_sigma,
        output_jet_collection=None):

    ca = ComponentAccumulator()

    if jet_collection.startswith("AntiKt4EMPFlowJets"):
        jc = JC.PFlow
    elif jet_collection.startswith("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"):
        jc = JC.LargeR
    else:
        raise ValueError(f'Unsupported jet colleciton: {jet_collection}')

    jetdef = jet_collection.replace("Jets", "")
    if not output_jet_collection:
        output_jet_collection = f'{jet_collection}Sys'

    jetUncertTool = CompFactory.JetUncertaintiesTool(
        ConfigFile={
            JC.PFlow: "rel21/Summer2019/R4_SR_Scenario1_SimpleJER.config",
            JC.LargeR: "rel21/Fall2022/R10_CategoryReduction.config"
        }[jc],
        MCType="MC16",
        IsData=False,
        JetDefinition=jetdef
    )
    # Check the configs here:
    # /GroupData/JetUncertainties/CalibArea-08/rel21/Summer2019/. The
    # names of the systematics can be found there.  the name of the
    # jet systematics should be of this format:
    # JET_Flavor_Response.The c++ implementation is rather minimal so
    # that the user needs to ensure the correct systematic name is
    # used.
    jetCalibTool = CompFactory.JetCalibrationTool(
        ConfigFile={
            JC.PFlow: "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config",
            JC.LargeR: "JES_MC16recommendation_R10_UFO_CSSK_SoftDrop_JMS_Insitu_30Sep2022.config"
        }[jc],
        JetCollection = jetdef,
        IsData=False,
        CalibSequence="JetArea_Residual_EtaJES_GSC_Smear"
    )

    # this doesn't seem needed
    ca.addPublicTool(jetUncertTool)
    ca.addPublicTool(jetCalibTool)

    jetSysAlg = CompFactory.JetSystematicsAlg("JetSysAlg")
    jetSysAlg.jet_collection = jet_collection
    jetSysAlg.output_jet_collection = output_jet_collection
    jetSysAlg.systematic_variations  = sys_list
    jetSysAlg.sigma  = jet_sigma
    jetSysAlg.jet_uncert_tool = jetUncertTool
    jetSysAlg.jet_calib_tool = jetCalibTool

    ca.addEventAlgo(jetSysAlg)
    return ca

