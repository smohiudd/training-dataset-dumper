from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from FTagDumper.trackUtil import applyTrackSys
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagConfig import RetagRenameInputContainerCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleFixedConeAssociationAlgCfg


def AddTrackSys_largeR(
    cfgFlags,
    track_collection="InDetTrackParticles",
    sys_list=[],
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets",    
):

    ca = ComponentAccumulator()
    track_collection="InDetTrackParticles"
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"
    
    print('APPLYING SYSTEMATIC:', sys_list)
    if sys_list:
        ca.merge(applyTrackSys(sys_list, track_collection , jet_collection))
        track_collection = "InDetTrackParticles_Sys"


    
    ca.merge(BTagTrackAugmenterAlgCfg(
            cfgFlags,
            TrackCollection=track_collection,
            PrimaryVertexCollectionName='PrimaryVertices',
        ))
  
    
    jet_collection_nosuffix = "AntiKt10UFOCSSKSoftDropBeta100Zcut10"
    addRenameMaps=['xAOD::TrackParticleAuxContainer#' + track_collection + '.btagIp_invalidIp->' + track_collection + '.btagIp_invalidIp_' + '_retag']
    ca.merge(RetagRenameInputContainerCfg("_retag", jet_collection_nosuffix, tracksKey=track_collection, addRenameMaps=addRenameMaps))
    ca.merge(JetTagCalibCfg(cfgFlags))
    

    fixedConeRadius = 1.0
    ca.merge(JetParticleFixedConeAssociationAlgCfg(
        cfgFlags,
        fixedConeRadius,
        JetCollection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets",
        InputParticleCollection=track_collection,
        OutputParticleDecoration="DeltaRTrackLink")
    )
    
    return ca
