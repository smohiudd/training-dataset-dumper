from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

VARIATIONS = {'TRK_RES_D0_MEAS', 'TRK_RES_Z0_MEAS'}

###############################################################
# Function to merge the standard and lrt tracks
###############################################################
def LRTMerger():
    ca = ComponentAccumulator()
    tool = CompFactory.DerivationFramework.TrackParticleMerger(name = "MergeLRTAndStandard", TrackParticleLocation = ["InDetTrackParticles", "InDetLargeD0TrackParticles"], OutputTrackParticleLocation = "InDetWithLRTTrackParticles", CreateViewColllection  = True)
    
    ca.addPublicTool(tool)
    
    LRTMergeAug = CompFactory.DerivationFramework.CommonAugmentation("InDetLRTMerge", AugmentationTools = tool)

    ca.addEventAlgo(LRTMergeAug)
    return ca

###############################################################
# Function to apply tracking  systematics
###############################################################
def applyTrackSys(sys_list, input_track, jet, output_tracks=None):

    if output_tracks is None:
        output_tracks = "InDetTrackParticles_Sys"

    ca = ComponentAccumulator()
    # Please use prime numbers as the seeds to ensure higher scientific qualities
    TrackSmearingTool = CompFactory.InDet.InDetTrackSmearingTool()
    TrackSmearingTool.Seed = 2

    TrackBiasingTool = CompFactory.InDet.InDetTrackBiasingTool()

    JetTrackFilterTool = CompFactory.InDet.JetTrackFilterTool()
    JetTrackFilterTool.Seed = 3

    TrackTruthFilterTool = CompFactory.InDet.InDetTrackTruthFilterTool()
    TrackTruthFilterTool.Seed = 5

    # You can provide a list of systematics and in the end it will
    # give us one container with all systematic uncertainties applied
    trackSysAlg = CompFactory.TrackSystematicsAlg(
        '_'.join(["TrackSysAlg", input_track, jet] + sys_list))
    trackSysAlg.systematic_variations = sys_list
    trackSysAlg.track_collection = input_track
    trackSysAlg.jet_collection = jet
    trackSysAlg.track_smearing_tool = TrackSmearingTool
    trackSysAlg.track_biasing_tool = TrackBiasingTool
    trackSysAlg.track_truth_filter_tool = TrackTruthFilterTool
    trackSysAlg.jet_track_filter_tool = JetTrackFilterTool
    trackSysAlg.output_track_collection = output_tracks
    ca.addEventAlgo(trackSysAlg)
    return ca
