# see https://ftag-docs.docs.cern.ch/activities/trackless/



# https://its.cern.ch/jira/browse/ATLFTAGDPD-376
### sample has latest Hit Variables (HitContainedInTrack/HitElementLink included)
### only Zprime so far!
mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.merge.DAOD_FTAG1.e8514_e8528_s4162_s4114_r15209_p6023


# https://its.cern.ch/jira/browse/ATLFTAGDPD-345
### ttbar & Zprime samples
### sample doesn't have variables called  HitContainedInTrack & HitElementLink included!
#mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r14736_r14672_p5700
#mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3778_r13258_r13146_p5700
