#!/usr/bin/env python

"""

Dumper configuration to regress jet energy

"""

import sys
from FTagDumper import dumper

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def jetMatcherCfg(
        target_jets,
        source_jets,
        dR='deltaRToMatchedTruthJet',
    ):
    ca = ComponentAccumulator()
    matcher = CompFactory.JetMatcherAlg(
        name=f'Matcher_{target_jets}_from_{source_jets}',
        targetJet=target_jets,
        sourceJets=[source_jets],
        floatsToCopy={
            'pt': 'truthJetPt',
            'eta': 'truthJetEta'
        },
        iparticlesToCopy={},
        dR=dR
    )
    ca.addEventAlgo(matcher)
    return ca


def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    jet_name = 'AntiKt4EMPFlow'

    ca = dumper.getMainConfig(cfgFlags, args)
    ca.merge(jetMatcherCfg(
        target_jets=f'{jet_name}Jets',
        source_jets='AntiKt4TruthJets',
    ))
    # this is needed to add the jet -> btag link to jetm2, which was
    # missing
    ca.addEventAlgo(CompFactory.BTagToJetLinkerAlg(
        'jetToBTag',
        newLink=f'BTagging_{jet_name}.jetLink',
        oldLink=f'{jet_name}Jets.btaggingLink'
    ))

    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
