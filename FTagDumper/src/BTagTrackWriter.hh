#ifndef BTAG_TRACK_WRITER_HH
#define BTAG_TRACK_WRITER_HH

#include "xAODTracking/TrackParticleFwd.h"
#include "xAODJet/JetFwd.h"

// Standard Library things
#include <string>
#include <vector>
#include <memory>

namespace H5 {
  class Group;
}

class JetConstituentWriterConfig;
class JetTrackWriter;

class BTagTrackWriter
{
public:
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  BTagTrackWriter(
    H5::Group& output_file,
    const JetConstituentWriterConfig&);
  ~BTagTrackWriter();
  BTagTrackWriter(BTagTrackWriter&) = delete;
  BTagTrackWriter operator=(BTagTrackWriter&) = delete;
  BTagTrackWriter(BTagTrackWriter&&);
  void write(const BTagTrackWriter::Tracks& tracks, const xAOD::Jet& jet);
private:
  std::unique_ptr<JetTrackWriter> m_writer;
};

#endif
