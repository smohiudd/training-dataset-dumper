#ifndef HEL_DECORATOR_ALG_HH
#define HEL_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "xAODTracking/TrackParticleContainer.h"



/*
   This is a copy of 
   DerivationFrameworkMCTruth/​TruthClassificationDecorator.h
   to avoid requiring full Athena to access the 
   DerivationFrameworkMCTruth package. 
   
   A cleaner workaround would be to move the 
   ​TruthClassificationDecorator class to the 
   MCTruthClassifier package.
*/

class HitELDecoratorAlg :  public AthReentrantAlgorithm { 
public:
  
  /** Constructors */
  HitELDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);
  
  /** Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&) const override ;


private:



  SG::ReadHandleKey<xAOD::TrackParticleContainer>   m_tracks{this,"TrackParticles", "InDetTrackParticles", "Track Particles"};
  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetPixelCluster{this,"JetAssociatedPixelClusters", "JetAssociatedPixelClusters", "PixelClusters"};
  SG::ReadHandleKey<xAOD::TrackMeasurementValidationContainer>   m_JetSCTCluster{this,"JetAssociatedSCTClusters", "JetAssociatedSCTClusters", "SCTClusters"};

  SG::WriteDecorHandleKey< xAOD::TrackParticleContainer > m_track_ID { this, "TrackParticleID", m_tracks, "TrackID"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetSCTClusterTrackID { this, "JetAssociatedSCTClustersTrackID", m_JetSCTCluster, "TrackID"};
  SG::WriteDecorHandleKey< xAOD::TrackMeasurementValidationContainer > m_JetPixelClusterTrackID { this, "JetAssociatedPixelClustersTrackID", m_JetPixelCluster, "TrackID"};



  template <typename T>
  using Acc = SG::AuxElement::ConstAccessor<T>;
  Acc<int> m_HitCont;
  Acc<std::vector<ElementLink<xAOD::TrackParticleContainer>>> m_HitLink;
  Acc<unsigned int> m_TrackID;

};


#endif